#ifndef EMPTYROAD_H
#define EMPTYROAD_H
#include <string>

using namespace std;

class emptyroad
{
    public:
        emptyroad();
        void updateRoad();

    protected:

    private:
        string situation[16];

};

#endif // EMPTYROAD_H
